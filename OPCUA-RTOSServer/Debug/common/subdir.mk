################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../common/lpc18xx_43xx_emac.c \
../common/lpc18xx_43xx_systick_arch.c \
../common/lpc_debug.c \
../common/sys_arch_freertos.c 

OBJS += \
./common/lpc18xx_43xx_emac.o \
./common/lpc18xx_43xx_systick_arch.o \
./common/lpc_debug.o \
./common/sys_arch_freertos.o 

C_DEPS += \
./common/lpc18xx_43xx_emac.d \
./common/lpc18xx_43xx_systick_arch.d \
./common/lpc_debug.d \
./common/sys_arch_freertos.d 


# Each subdirectory must supply rules for building sources it contributes
common/%.o: ../common/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -D__USE_LPCOPEN -DCORE_M3 -I"/home/chris/GitHub/OPCUA-RTOS-Server/lpc_chip_18xx/inc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/lpc_chip_18xx/inc/config_18xx" -I"/home/chris/GitHub/OPCUA-RTOS-Server/lpc_board_keil_mcb_1857/inc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/inc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/lwip/include" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/lwip/include/ipv4" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/common" -I"/home/chris/GitHub/OPCUA-RTOS-Server/freertos/inc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/freertos/inc_arm" -I"/home/chris/GitHub/OPCUA-RTOS-Server/freertos/inc_freertoslpc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/src/echo" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


