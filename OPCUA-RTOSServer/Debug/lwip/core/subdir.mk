################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lwip/core/def.c \
../lwip/core/dhcp.c \
../lwip/core/dns.c \
../lwip/core/init.c \
../lwip/core/mem.c \
../lwip/core/memp.c \
../lwip/core/netif.c \
../lwip/core/pbuf.c \
../lwip/core/raw.c \
../lwip/core/stats.c \
../lwip/core/sys.c \
../lwip/core/tcp.c \
../lwip/core/tcp_in.c \
../lwip/core/tcp_out.c \
../lwip/core/timers.c \
../lwip/core/udp.c 

OBJS += \
./lwip/core/def.o \
./lwip/core/dhcp.o \
./lwip/core/dns.o \
./lwip/core/init.o \
./lwip/core/mem.o \
./lwip/core/memp.o \
./lwip/core/netif.o \
./lwip/core/pbuf.o \
./lwip/core/raw.o \
./lwip/core/stats.o \
./lwip/core/sys.o \
./lwip/core/tcp.o \
./lwip/core/tcp_in.o \
./lwip/core/tcp_out.o \
./lwip/core/timers.o \
./lwip/core/udp.o 

C_DEPS += \
./lwip/core/def.d \
./lwip/core/dhcp.d \
./lwip/core/dns.d \
./lwip/core/init.d \
./lwip/core/mem.d \
./lwip/core/memp.d \
./lwip/core/netif.d \
./lwip/core/pbuf.d \
./lwip/core/raw.d \
./lwip/core/stats.d \
./lwip/core/sys.d \
./lwip/core/tcp.d \
./lwip/core/tcp_in.d \
./lwip/core/tcp_out.d \
./lwip/core/timers.d \
./lwip/core/udp.d 


# Each subdirectory must supply rules for building sources it contributes
lwip/core/%.o: ../lwip/core/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -D__USE_LPCOPEN -DCORE_M3 -I"/home/chris/GitHub/OPCUA-RTOS-Server/lpc_chip_18xx/inc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/lpc_chip_18xx/inc/config_18xx" -I"/home/chris/GitHub/OPCUA-RTOS-Server/lpc_board_keil_mcb_1857/inc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/inc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/lwip/include" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/lwip/include/ipv4" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/common" -I"/home/chris/GitHub/OPCUA-RTOS-Server/freertos/inc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/freertos/inc_arm" -I"/home/chris/GitHub/OPCUA-RTOS-Server/freertos/inc_freertoslpc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/src/echo" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


