################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lwip/core/ipv4/autoip.c \
../lwip/core/ipv4/icmp.c \
../lwip/core/ipv4/igmp.c \
../lwip/core/ipv4/inet.c \
../lwip/core/ipv4/inet_chksum.c \
../lwip/core/ipv4/ip.c \
../lwip/core/ipv4/ip_addr.c \
../lwip/core/ipv4/ip_frag.c 

OBJS += \
./lwip/core/ipv4/autoip.o \
./lwip/core/ipv4/icmp.o \
./lwip/core/ipv4/igmp.o \
./lwip/core/ipv4/inet.o \
./lwip/core/ipv4/inet_chksum.o \
./lwip/core/ipv4/ip.o \
./lwip/core/ipv4/ip_addr.o \
./lwip/core/ipv4/ip_frag.o 

C_DEPS += \
./lwip/core/ipv4/autoip.d \
./lwip/core/ipv4/icmp.d \
./lwip/core/ipv4/igmp.d \
./lwip/core/ipv4/inet.d \
./lwip/core/ipv4/inet_chksum.d \
./lwip/core/ipv4/ip.d \
./lwip/core/ipv4/ip_addr.d \
./lwip/core/ipv4/ip_frag.d 


# Each subdirectory must supply rules for building sources it contributes
lwip/core/ipv4/%.o: ../lwip/core/ipv4/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -D__USE_LPCOPEN -DCORE_M3 -I"/home/chris/GitHub/OPCUA-RTOS-Server/lpc_chip_18xx/inc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/lpc_chip_18xx/inc/config_18xx" -I"/home/chris/GitHub/OPCUA-RTOS-Server/lpc_board_keil_mcb_1857/inc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/inc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/lwip/include" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/lwip/include/ipv4" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/common" -I"/home/chris/GitHub/OPCUA-RTOS-Server/freertos/inc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/freertos/inc_arm" -I"/home/chris/GitHub/OPCUA-RTOS-Server/freertos/inc_freertoslpc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/src/echo" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


