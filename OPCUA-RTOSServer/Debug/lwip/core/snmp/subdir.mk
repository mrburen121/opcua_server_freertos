################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lwip/core/snmp/asn1_dec.c \
../lwip/core/snmp/asn1_enc.c \
../lwip/core/snmp/mib2.c \
../lwip/core/snmp/mib_structs.c \
../lwip/core/snmp/msg_in.c \
../lwip/core/snmp/msg_out.c 

OBJS += \
./lwip/core/snmp/asn1_dec.o \
./lwip/core/snmp/asn1_enc.o \
./lwip/core/snmp/mib2.o \
./lwip/core/snmp/mib_structs.o \
./lwip/core/snmp/msg_in.o \
./lwip/core/snmp/msg_out.o 

C_DEPS += \
./lwip/core/snmp/asn1_dec.d \
./lwip/core/snmp/asn1_enc.d \
./lwip/core/snmp/mib2.d \
./lwip/core/snmp/mib_structs.d \
./lwip/core/snmp/msg_in.d \
./lwip/core/snmp/msg_out.d 


# Each subdirectory must supply rules for building sources it contributes
lwip/core/snmp/%.o: ../lwip/core/snmp/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -D__USE_LPCOPEN -DCORE_M3 -I"/home/chris/GitHub/OPCUA-RTOS-Server/lpc_chip_18xx/inc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/lpc_chip_18xx/inc/config_18xx" -I"/home/chris/GitHub/OPCUA-RTOS-Server/lpc_board_keil_mcb_1857/inc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/inc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/lwip/include" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/lwip/include/ipv4" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/common" -I"/home/chris/GitHub/OPCUA-RTOS-Server/freertos/inc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/freertos/inc_arm" -I"/home/chris/GitHub/OPCUA-RTOS-Server/freertos/inc_freertoslpc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/src/echo" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


