################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lwip/netif/ppp/auth.c \
../lwip/netif/ppp/chap.c \
../lwip/netif/ppp/chpms.c \
../lwip/netif/ppp/fsm.c \
../lwip/netif/ppp/ipcp.c \
../lwip/netif/ppp/lcp.c \
../lwip/netif/ppp/magic.c \
../lwip/netif/ppp/md5.c \
../lwip/netif/ppp/pap.c \
../lwip/netif/ppp/ppp.c \
../lwip/netif/ppp/ppp_oe.c \
../lwip/netif/ppp/randm.c \
../lwip/netif/ppp/vj.c 

OBJS += \
./lwip/netif/ppp/auth.o \
./lwip/netif/ppp/chap.o \
./lwip/netif/ppp/chpms.o \
./lwip/netif/ppp/fsm.o \
./lwip/netif/ppp/ipcp.o \
./lwip/netif/ppp/lcp.o \
./lwip/netif/ppp/magic.o \
./lwip/netif/ppp/md5.o \
./lwip/netif/ppp/pap.o \
./lwip/netif/ppp/ppp.o \
./lwip/netif/ppp/ppp_oe.o \
./lwip/netif/ppp/randm.o \
./lwip/netif/ppp/vj.o 

C_DEPS += \
./lwip/netif/ppp/auth.d \
./lwip/netif/ppp/chap.d \
./lwip/netif/ppp/chpms.d \
./lwip/netif/ppp/fsm.d \
./lwip/netif/ppp/ipcp.d \
./lwip/netif/ppp/lcp.d \
./lwip/netif/ppp/magic.d \
./lwip/netif/ppp/md5.d \
./lwip/netif/ppp/pap.d \
./lwip/netif/ppp/ppp.d \
./lwip/netif/ppp/ppp_oe.d \
./lwip/netif/ppp/randm.d \
./lwip/netif/ppp/vj.d 


# Each subdirectory must supply rules for building sources it contributes
lwip/netif/ppp/%.o: ../lwip/netif/ppp/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -D__USE_LPCOPEN -DCORE_M3 -I"/home/chris/GitHub/OPCUA-RTOS-Server/lpc_chip_18xx/inc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/lpc_chip_18xx/inc/config_18xx" -I"/home/chris/GitHub/OPCUA-RTOS-Server/lpc_board_keil_mcb_1857/inc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/inc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/lwip/include" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/lwip/include/ipv4" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/common" -I"/home/chris/GitHub/OPCUA-RTOS-Server/freertos/inc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/freertos/inc_arm" -I"/home/chris/GitHub/OPCUA-RTOS-Server/freertos/inc_freertoslpc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/src/echo" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


