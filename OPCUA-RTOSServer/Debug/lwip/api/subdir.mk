################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lwip/api/api_lib.c \
../lwip/api/api_msg.c \
../lwip/api/err.c \
../lwip/api/netbuf.c \
../lwip/api/netdb.c \
../lwip/api/netifapi.c \
../lwip/api/sockets.c \
../lwip/api/tcpip.c 

OBJS += \
./lwip/api/api_lib.o \
./lwip/api/api_msg.o \
./lwip/api/err.o \
./lwip/api/netbuf.o \
./lwip/api/netdb.o \
./lwip/api/netifapi.o \
./lwip/api/sockets.o \
./lwip/api/tcpip.o 

C_DEPS += \
./lwip/api/api_lib.d \
./lwip/api/api_msg.d \
./lwip/api/err.d \
./lwip/api/netbuf.d \
./lwip/api/netdb.d \
./lwip/api/netifapi.d \
./lwip/api/sockets.d \
./lwip/api/tcpip.d 


# Each subdirectory must supply rules for building sources it contributes
lwip/api/%.o: ../lwip/api/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -D__USE_LPCOPEN -DCORE_M3 -I"/home/chris/GitHub/OPCUA-RTOS-Server/lpc_chip_18xx/inc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/lpc_chip_18xx/inc/config_18xx" -I"/home/chris/GitHub/OPCUA-RTOS-Server/lpc_board_keil_mcb_1857/inc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/inc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/lwip/include" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/lwip/include/ipv4" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/common" -I"/home/chris/GitHub/OPCUA-RTOS-Server/freertos/inc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/freertos/inc_arm" -I"/home/chris/GitHub/OPCUA-RTOS-Server/freertos/inc_freertoslpc" -I"/home/chris/GitHub/OPCUA-RTOS-Server/OPCUA-RTOSServer/src/echo" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


