/*
 * Author:	Chris Ketterink
 * Date: 	21-06-2019
 * Project:	OPC UA Linux server application
 * Version:	see GitHub repository for version control
 * GitHub:	https://github.com/mrburen121/OPCUA-UbuntuServer (Private Repository)
 *
 * File:	global.c
 * File description:
 * global variables that are user in the OPC UA server application.
 *
 */

#ifndef GLOBAL_H_
#define GLOBAL_H_

//System library's
#include <signal.h>
#include <stdio.h>
//#include <unistd.h>
#include <string.h>
#include <stdlib.h>
//#include <errno.h>


//lwip/tcp
#include "lwiptcpInit.h"
#include "opcuaTasks.h"
// Project library's
//#include "open62541.h"


#ifdef __cplusplus
extern "C" {
#endif



#ifdef __cplusplus
} // extern "C"
#endif

#endif /* GLOBAL_H_ */
