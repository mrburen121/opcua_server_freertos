/*
 * lwiptcpInit.h
 *
 *  Created on: Jul 15, 2019
 *      Author: chris
 */

#ifndef LWIPTCPINIT_H_
#define LWIPTCPINIT_H_

#include "lwip/init.h"
#include "lwip/opt.h"
#include "lwip/sys.h"
#include "lwip/memp.h"
#include "lwip/tcpip.h"
#include "lwip/ip_addr.h"
#include "lwip/netif.h"
#include "lwip/timers.h"
#include "netif/etharp.h"

#if LWIP_DHCP
#include "lwip/dhcp.h"
#endif

#include "board.h"
#include "arch/lpc18xx_43xx_emac.h"
#include "arch/lpc_arch.h"
#include "arch/sys_arch.h"
#include "lpc_phy.h" /* For the PHY monitor support */
#include "tcpecho.h"

#ifdef __cplusplus
extern "C" {
#endif

void prvSetupHardware(void);

void vSetupIFTask(void *pvParameters);

void tcpip_init_done_signal(void *arg);

void msDelay(uint32_t ms);

#ifdef __cplusplus
} // extern "C"
#endif

#endif /* LWIPTCPINIT_H_ */
