/*
 * opcuaTasks.h
 *
 *  Created on: Jul 15, 2019
 *      Author: chris
 */

#ifndef OPCUATASKS_H_
#define OPCUATASKS_H_

void vLEDTask1(void *pvParameters);

void vLEDTask2(void *pvParameters);

void vUARTTask(void *pvParameters);

#endif /* OPCUATASKS_H_ */
