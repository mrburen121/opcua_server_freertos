# OPCUA_Server_freertos

Folder structure:
- freertos: FreeRTOS V10.2.0
- lpc_board_keil_mcb: board files MCB1857 development board
- lpc_chip_18xx: chip files LPC1857
- OPCUA_RTOSServer: main project with source files and this project should contains the open62541.c/h files.
